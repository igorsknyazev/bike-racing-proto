# Hill Climb, proto

[Read task description](TASK.md)

[Watch example video on Youtube](https://www.youtube.com/watch?v=CMJ5LhclcAI)

[Download demo apk from Google Drive](https://drive.google.com/file/d/1sAO-PkSoZ_kCpUenahBsdYGZjABF2xdL)

## Known issues

### When bike touches ground with it's bottom part (zone between wheels), it dies.

This can be solved by adding "driver" with mortal collider. Now I use bike collider to detect death.
  
### Text "Wheelie!" flashes often. This happens because wheels detect ground too accurate.

  Ground isn't smooth and suspension is too sensitive,
  
  so wheels are often in the flight state.
  
  It can be solved by adding bigger trigger colliders for wheels.
  
  Or by adding additional trigger collider to ground sectors.
  
  Or by raycasting from wheels to Vector2.down (will not work when moving uphill or downhill).
  
## Questions

* Why not use singleton or service provider patter to car?
    - Because there are too little components reference to car object. So I've decided to use it by reference for prototype.
* Why not use universal object pool realization with generics?
    - Because there is only one consumer of pool system (ground segments spawner). So I've made specific realization.