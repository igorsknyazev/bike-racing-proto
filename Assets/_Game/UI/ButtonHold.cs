using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace _Game.UI
{
	public class ButtonHold : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
	{
		public UnityEvent OnButtonHold;
		public UnityEvent OnButtonReleased;
		
		public void OnPointerDown(PointerEventData eventData)
		{
			OnButtonHold?.Invoke();
		}

		public void OnPointerUp(PointerEventData eventData)
		{
			OnButtonReleased?.Invoke();
		}
	}
}