using System;
using System.Collections.Generic;
using System.Linq;
using _Game.Ground;
using _Game.Ground.SpawnPool;
using Cinemachine;
using UnityEngine;
using Random = UnityEngine.Random;

namespace _Game
{
	public struct SegmentsChunk
	{
		// Segments are spawned from p1 to p2
		public Vector2 p1;
		public Vector2 p2;

		public List<SurfaceSegment> segments;

		// p0 and p3 are using in Catmull-Rom algoritm
		public Vector2 p0;
		public Vector2 p3;

		public bool IsVisible(float leftVisibleLimitX, float rightVisibleLimitX)
		{
			return p1.x >= leftVisibleLimitX && p2.x <= rightVisibleLimitX;
		}

		public bool Equal(Vector2 _p1, Vector2 _p2)
		{
			return _p1 == p1 && _p2 == p2;
		}
	}
	
	public class SurfaceGenerator : MonoBehaviour
	{
		[SerializeField]
		private Camera worldCamera;

		// Distance between two control points
		[SerializeField]
		private float xStepControl = 5f;

		// Additional horizontal space for control points spawn.
		[SerializeField]
		private float horizontalAdditionSpawnExtent = 10;
		// Y range for top side of segment
		[SerializeField]
		private float minVerticalExtent = -3;
		[SerializeField]
		private float maxVerticalExtent = 3;
		// Bottom side position (Y) of segment
		[SerializeField]
		private float segmentsYCoordinate = -20f;
		
		[SerializeField]
		private SegmentsSpawnPool segmentsSpawnPool;

		private float _cameraExtentWidth;
		
		private List<SegmentsChunk> _segmentsChunks = new List<SegmentsChunk>();

		private void Start()
		{
			var viewportMin = ViewportMin();
			var viewportMax = -viewportMin;

			_cameraExtentWidth = viewportMax.x;
			
			GenerateControlPointsFromTill(GetLeftSpawnPoint(), GetRightSpawnPoint(), xStepControl);
		}

		private float GetLeftSpawnPoint()
		{
			return worldCamera.transform.position.x - _cameraExtentWidth - horizontalAdditionSpawnExtent;
		}

		private float GetRightSpawnPoint()
		{
			return worldCamera.transform.position.x + _cameraExtentWidth + horizontalAdditionSpawnExtent;
		}

		private LinkedList<Vector2> _controlPoints = new LinkedList<Vector2>();

		private void GenerateControlPointsFromTill(float fromX, float toX, float step)
		{
			if (fromX < toX && step > 0)
			{
				while (fromX < toX)
				{
					var newPoint = new Vector2(fromX, Random.Range(minVerticalExtent, maxVerticalExtent));
					_controlPoints.AddLast(newPoint);
					
					fromX += step;
				}
			}
			else if (fromX > toX && step < 0)
			{
				while (fromX > toX)
				{
					var newPoint = new Vector2(fromX, Random.Range(minVerticalExtent, maxVerticalExtent));
					_controlPoints.AddFirst(newPoint);
					
					fromX += step;
				}
			}
		}

		#region Gizmos visualization

		private void OnDrawGizmos()
		{
			foreach (var controlPoint in _controlPoints)
			{
				Gizmos.DrawSphere(controlPoint, 0.5f);
			}

			var controlPointsList = _controlPoints.ToList();

			for (int i = 1; i < controlPointsList.Count - 2; i++)
			{
				DrawCatmullRom(
					controlPointsList[i - 1],
					controlPointsList[i],
					controlPointsList[i + 1],
					controlPointsList[i + 2]
					);
			}
		}

		private void DrawCatmullRom(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
		{
			// It draws spline through points p1 and p2, using p0 and p3 as target points

			var step = 0.1f;

			var prevPoint = p1;

			for (float t = 0; t <= 1; t += step)
			{
				var nextPoint = 0.5f * ((2 * p1)
				                        + (-p0 + p2) * t
				                        + (2 * p0 - 5 * p1 + 4 * p2 - p3) * t * t
				                        + (-p0 + 3 * p1 - 3 * p2 + p3) * t * t * t);
				
				Gizmos.DrawLine(prevPoint, nextPoint);
				prevPoint = nextPoint;
			}
			
			Gizmos.DrawLine(prevPoint, p2);
		}

		#endregion

		private void Update()
		{
			GenerateRequiredControlPoints();

			DespawnInvisibleChunks();
			
			// Take all required control points for graph (they have X coordinate in spawn range)
			var targetControlPoints =
				_controlPoints.Where(cp => cp.x >= GetLeftSpawnPoint() && cp.x <= GetRightSpawnPoint());

			SpawnRequiredChunks(targetControlPoints.ToList());
		}

		private void GenerateRequiredControlPoints()
		{
			// Generate new control points when move camera
			var firstPoint = _controlPoints.First.Value;
			var lastPoint = _controlPoints.Last.Value;

			if (GetRightSpawnPoint() + xStepControl > lastPoint.x)
			{
				// generate new points to right
				GenerateControlPointsFromTill(lastPoint.x + xStepControl, GetRightSpawnPoint() + xStepControl, xStepControl);
			}

			if (GetLeftSpawnPoint() - xStepControl < firstPoint.x)
			{
				// generate new points to left
				GenerateControlPointsFromTill(firstPoint.x - xStepControl, GetLeftSpawnPoint() - xStepControl, -xStepControl);
			}
		}

		private void SpawnRequiredChunks(List<Vector2> targetControlPoints)
		{
			for (int i = 1; i < targetControlPoints.Count - 2; i++)
			{
				if (_segmentsChunks.Any(chunk => chunk.Equal(targetControlPoints[i], targetControlPoints[i + 1])))
				{
					continue;
				}
				
				var newChunk = SpawnChunkSegments(
					targetControlPoints[i - 1],
					targetControlPoints[i],
					targetControlPoints[i + 1],
					targetControlPoints[i + 2]
				);
				
				_segmentsChunks.Add(newChunk);
			}
		}
		
		private SegmentsChunk SpawnChunkSegments(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3)
		{
			var resultChunk = new SegmentsChunk()
			{
				p0 = p0,
				p1 = p1,
				p2 = p2,
				p3 = p3,
				segments = new List<SurfaceSegment>()
			};

			var step = 0.1f;

			var prevPoint = p1;

			for (float t = 0; t <= 1; t += step)
			{
				// Catmull-ROM algorithm magic
				var nextPoint = 0.5f * ((2 * p1)
				                        + (-p0 + p2) * t
				                        + (2 * p0 - 5 * p1 + 4 * p2 - p3) * t * t
				                        + (-p0 + 3 * p1 - 3 * p2 + p3) * t * t * t);
				
				resultChunk.segments.Add(SpawnSegmentWithHeights(prevPoint, nextPoint));
				prevPoint = nextPoint;
			}
			
			resultChunk.segments.Add(SpawnSegmentWithHeights(prevPoint, p2));

			return resultChunk;
		}
		
		private SurfaceSegment SpawnSegmentWithHeights(Vector2 leftTop, Vector2 rightTop)
		{
			var segment = segmentsSpawnPool.Spawn();
			segment.transform.position = new Vector3(leftTop.x, segmentsYCoordinate, 0);
			segment.Init(rightTop.x - leftTop.x, leftTop.y - segmentsYCoordinate,
				rightTop.y - segmentsYCoordinate);

			return segment;
		}

		private void DespawnInvisibleChunks()
		{
			var chunksToDespawn =
				_segmentsChunks.Where(chunk => !chunk.IsVisible(GetLeftSpawnPoint(), GetRightSpawnPoint()));

			foreach (var chunkToDespawn in chunksToDespawn)
			{
				segmentsSpawnPool.Despawn(chunkToDespawn.segments);
			}

			_segmentsChunks =
				_segmentsChunks.Where(chunk => chunk.IsVisible(GetLeftSpawnPoint(), GetRightSpawnPoint())).ToList();
		}

		private Vector2 ViewportMin()
		{
			return worldCamera.ViewportToWorldPoint(Vector2.zero);
		}
	}
}