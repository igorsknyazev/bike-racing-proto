using System;
using _Game.Player;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.UI
{
	public class TextWheelie : MonoBehaviour
	{
		// I use this to avoid frequent wheelie blinking
		public float wheelieInertiaTime = 0.2f;
		
		public Text textField;
		public WheelsTouchGroundDetector wheelsTouchGroundDetector;

		private DateTime _lastWheelieChanged = DateTime.Now;
		
		private void Start()
		{
			wheelsTouchGroundDetector.OnWheelie += Redraw;
		}

		private void Redraw(bool isWheelie)
		{
			if ((DateTime.Now - _lastWheelieChanged).TotalSeconds < wheelieInertiaTime)
			{
				return;
			}
			
			textField.enabled = isWheelie;
			_lastWheelieChanged = DateTime.Now;
		}
	}
}