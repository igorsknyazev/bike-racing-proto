using _Game.Player;
using UnityEngine;
using UnityEngine.UI;

namespace _Game.UI
{
	public class TextDistance : MonoBehaviour
	{
		public CarDistanceMeter carDistanceMeter;
		public Text textField;

		private void Update()
		{
			textField.text = $"{carDistanceMeter.TraveledDistance():N2}";
		}
	}
}