using UnityEngine;

namespace _Game.Player
{
	public class PlayerControl : MonoBehaviour
	{
		[SerializeField]
		private CarMovement carMovement;

		private float _accelerateVal = 0f;
		private float _brakeVal = 0f;

		public void SetAcceleratePower(float val)
		{
			_accelerateVal = val;
			ApplyAcceleration();
		}

		public void SetBrakePower(float val)
		{
			_brakeVal = val;
			ApplyAcceleration();
		}

		private void ApplyAcceleration()
		{
			var moveValue = _accelerateVal - _brakeVal;
			carMovement.Accelerate(moveValue);
		}
	}
}