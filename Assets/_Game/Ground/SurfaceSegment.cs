using _Game.Tools;
using UnityEngine;

namespace _Game.Ground
{
	public class SurfaceSegment : MonoBehaviour
	{
		[SerializeField]
		private MeshFilter meshFilter;
		[SerializeField]
		private PolygonCollider2D polygonCollider2D;

		private float _width;

		public void Init(float width, float heightLeft, float heightRight)
		{
			_width = width;

			Mesh mesh = new Mesh();

			Vector3[] vertices = new Vector3[4]
			{
				Vector3.zero,
				width * Vector3.right,
				heightLeft * Vector3.up,
				width * Vector3.right + heightRight * Vector3.up,
			};
			mesh.vertices = vertices;

			int[] tris = new int[6]
			{
				// Order vertices clockwise
				// Lower left
				0, 2, 1,
				// Upper right
				2, 3, 1
			};
			mesh.triangles = tris;

			mesh.RecalculateNormals();

			meshFilter.mesh = mesh;

			polygonCollider2D.points = vertices.ConvertToTwoDimensional();
		}

		public bool IsVisible(float cameraXPos, float horizontalExtent)
		{
			var pos = transform.position;
			return (pos.x - _width) >= (cameraXPos - horizontalExtent)
			       && pos.x <= (cameraXPos + horizontalExtent);
		}
	}
}