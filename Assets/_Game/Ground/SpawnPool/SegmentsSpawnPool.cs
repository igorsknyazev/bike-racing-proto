using System.Collections.Generic;
using UnityEngine;

namespace _Game.Ground.SpawnPool
{
	public class SegmentsSpawnPool : MonoBehaviour
	{
		[SerializeField]
		private SurfaceSegment prefabSurfaceSegment;
		
		private Queue<SurfaceSegment> _pooledSegments = new Queue<SurfaceSegment>();

		public SurfaceSegment Spawn()
		{
			if (_pooledSegments.Count > 0)
			{
				var segmentToSpawn = _pooledSegments.Dequeue();
				segmentToSpawn.gameObject.SetActive(true);
				return segmentToSpawn;
			}

			return Instantiate(prefabSurfaceSegment, transform);
		}

		public void Despawn(SurfaceSegment instance)
		{
			if (!_pooledSegments.Contains(instance))
			{
				_pooledSegments.Enqueue(instance);
			}
			
			instance.gameObject.SetActive(false);
		}

		public void Despawn(List<SurfaceSegment> instances)
		{
			foreach (var instance in instances)
			{
				Despawn(instance);
			}
		}
	}
}