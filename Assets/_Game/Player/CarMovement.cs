using UnityEngine;

namespace _Game.Player
{
	public class CarMovement : MonoBehaviour
	{
		[SerializeField]
		private float maxMotorSpeed;
		
		[SerializeField]
		private WheelJoint2D wheelRear;
		[SerializeField]
		private WheelJoint2D wheelFront;

		public void Accelerate(float value)
		{
			// if value < 0 - Brake on both wheels - it means motor speed == 0
			// if value == 0 - disable engine on both wheels - inertia slide
			// if value == 1 - use engine on both wheels

			if (value == 0)
			{
				ActivateMotorOnWheel(wheelRear, false);
				ActivateMotorOnWheel(wheelFront, false);
				return;
			}
			
			ActivateMotorOnWheel(wheelRear, true);
			ActivateMotorOnWheel(wheelFront, true);
			
			if (value < 0)
			{
				// Brake - give zero force on both wheels
				SetMotorSpeedForWheel(wheelRear, 0);
				SetMotorSpeedForWheel(wheelFront, 0);
			}
			else
			{
				// Accelerate
				SetMotorSpeedForWheel(wheelRear, maxMotorSpeed * value);
				SetMotorSpeedForWheel(wheelFront, maxMotorSpeed * value);
			}
		}

		private void SetMotorSpeedForWheel(WheelJoint2D wheel, float speed)
		{
			var motor = wheel.motor;
			motor.motorSpeed = speed;
			wheel.motor = motor;
		}

		private void ActivateMotorOnWheel(WheelJoint2D wheel, bool isMotorActive)
		{
			wheel.useMotor = isMotorActive;
		}
	}
}