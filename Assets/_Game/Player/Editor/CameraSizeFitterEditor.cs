using UnityEditor;
using UnityEngine;

namespace _Game.Player.Editor
{
	[CustomEditor(typeof(CameraSizeFitter))]
	public class CameraSizeFitterEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();

			if (GUILayout.Button("Fit"))
			{
				(target as CameraSizeFitter)?.Fit();
				if (GUI.changed)
				{
					EditorUtility.SetDirty(target);
				}
			}
		}
	}
}