using System;
using System.Collections.Generic;
using UnityEngine;

namespace _Game.Player
{
	public class WheelsTouchGroundDetector : MonoBehaviour
	{
		public Action<bool> OnWheelie;
		
		[SerializeField]
		private CircleCollider2D wheelFrontCollider;
		[SerializeField]
		private CircleCollider2D wheelRearCollider;

		private ContactFilter2D _contactFilter;

		private void Start()
		{
			int groundLayerMask = LayerMask.NameToLayer("Ground");
			_contactFilter = new ContactFilter2D
			{
				layerMask = groundLayerMask
			};
		}

		private void FixedUpdate()
		{
			List<ContactPoint2D> contacts = new List<ContactPoint2D>();
			int rearContactsAmount = wheelRearCollider.GetContacts(_contactFilter, contacts);
			int frontContactsAmount = wheelFrontCollider.GetContacts(_contactFilter, contacts);
			
			OnWheelie?.Invoke(rearContactsAmount > 0 ^ frontContactsAmount > 0);
		}
	}
}