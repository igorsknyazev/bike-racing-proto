using UnityEngine;

namespace _Game.Player
{
	public class CarDistanceMeter : MonoBehaviour
	{
		private float _posXStart;
		
		private void Start()
		{
			_posXStart = transform.position.x;
		}

		public float TraveledDistance()
		{
			return transform.position.x - _posXStart;
		}
	}
}