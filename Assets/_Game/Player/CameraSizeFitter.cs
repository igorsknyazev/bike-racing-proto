using Cinemachine;
using UnityEngine;

namespace _Game.Player
{
	public class CameraSizeFitter : MonoBehaviour
	{
		[SerializeField]
		private float requiredHalfWidth = 5;
		
		[SerializeField]
		private CinemachineVirtualCamera virtualCamera;
		[SerializeField]
		private Camera mainCamera;

		private readonly float targetAspect = 16f / 9f;

		private void Awake()
		{
			Fit();
		}

		public void Fit()
		{
			virtualCamera.m_Lens.OrthographicSize = requiredHalfWidth / virtualCamera.m_Lens.Aspect * targetAspect;
			// Main camera orthographic size is set by virtual camera. But only next frame.
			// But I need it NOW to draw segments correctly
			mainCamera.orthographicSize = virtualCamera.m_Lens.OrthographicSize;
		}
	}
}