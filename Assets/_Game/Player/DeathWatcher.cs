using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Game.Player
{
	public class DeathWatcher : MonoBehaviour
	{
		private int _groundLayer;

		private void Start()
		{
			_groundLayer = LayerMask.NameToLayer("Ground");
		}

		private void OnCollisionEnter2D(Collision2D other)
		{
			if (other.gameObject.layer == _groundLayer)
			{
				SceneManager.LoadScene(SceneManager.GetActiveScene().name);
			}
		}
	}
}