using UnityEngine;

namespace _Game.Tools
{
	public static class VectorExt
	{
		public static Vector2[] ConvertToTwoDimensional(this Vector3[] target)
		{
			Vector2[] result = new Vector2[target.Length];

			for (int i = 0; i < target.Length; i++)
			{
				result[i] = target[i];
			}

			return result;
		}
	}
}